from random import randint

import pygame
import scene_config
from config_script import WINDOW_SIZE
import group_config
from particle import createParticle

timeDamageHeart: int = 120
timeDamagePlayer: int = 100


def ColliderBall():
    global timeDamageHeart, timeDamagePlayer

    if timeDamagePlayer < 100:
        scene_config.player.image.set_alpha(100)
        timeDamagePlayer += 1

    key = pygame.key.get_pressed()
    for ball in group_config.ball_group:

        if timeDamageHeart >= 120:
            scene_config.player.imageHeart.set_alpha(300)
            if ball.rect.collidepoint(scene_config.player.rectHeart.center):
                scene_config.player.imageHeart.set_alpha(100)
                timeDamageHeart = 0
                scene_config.player.HeartCount -= 1
                ball.kill()
                scene_config.player.countSpit += 1
        else:
            timeDamageHeart += 1
        if scene_config.player.rect.colliderect(ball.rect) and timeDamageHeart > 0:
            if key[pygame.K_i]:
                ball.kill()
                scene_config.player.countSpit += 1
        if timeDamagePlayer >= 100:
            scene_config.player.image.set_alpha(300)
            if ball.rect.collidepoint(scene_config.player.rect.center):
                createParticle(scene_config.player.rect.centerx, scene_config.player.rect.centery,
                               "ASSETS/sprite/particle/blood.png", 15, 10, 2)
                timeDamagePlayer = 0
                scene_config.player.rect.x = WINDOW_SIZE[0] // 2
                scene_config.player.PlayerCount -= 1
        for crystal in group_config.crystal_group:
            if ball.rect.colliderect(crystal.rect):
                createParticle(crystal.rect.centerx, crystal.rect.centery, "ASSETS/sprite/particle/red_crystal_part.png"
                               , randint(5, 20), 5)
                scene_config.numberCrystal -= 1
                print(scene_config.numberCrystal)
                crystal.kill()
