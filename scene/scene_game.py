from random import randint

import pygame
import config_script
import scene_config
from scene_config import player
import group_config
from collider import ColliderBall

fontPlayer = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 29)

fontHeart = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 24)

playerTXT = fontPlayer.render("PLAYER: " + str(player.PlayerCount), False, (255, 255, 255))

heartTXT = fontHeart.render("HEART: " + str(player.HeartCount), False, (255, 0, 245))


def updateTXT():
    global playerTXT, heartTXT
    heartTXT = fontHeart.render("HEART: " + str(player.HeartCount), False, (255, 0, 245))
    playerTXT = fontPlayer.render("PLAYER: " + str(player.PlayerCount), False, (255, 0, 0))

    config_script.sc.blit(playerTXT, playerTXT.get_rect(center=(config_script.WINDOW_SIZE[0]//2, 25)))
    config_script.sc.blit(heartTXT, heartTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 55)))


def drawScene():
    config_script.sc.blit(player.image, player.rect)
    group_config.ball_group.draw(config_script.sc)
    group_config.crystal_group.draw(config_script.sc)
    group_config.particle_group.draw(config_script.sc)


def updateScene():
    player.update()
    group_config.ball_group.update()
    group_config.particle_group.update()


def scene():
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            config_script.running = False
    config_script.sc.fill((0, 0, 0))
    if scene_config.numberCrystal <= 0:
        for ball in group_config.ball_group:
            ball.kill()
        scene_config.LEVEL += 1
        scene_config.player.countSpit = 1
        scene_config.maxCreateCrystal += randint(3, 6)
        scene_config.changerLvl()
    ColliderBall()
    drawScene()
    updateScene()
    pygame.draw.line(config_script.sc, (255, 255, 255), (config_script.WINDOW_SIZE[0], 90), (0, 90))
    updateTXT()
    pygame.display.update()
    config_script.clock.tick(config_script.FPS)
