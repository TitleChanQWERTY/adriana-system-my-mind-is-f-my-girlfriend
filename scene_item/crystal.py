import pygame
from group_config import crystal_group


class Crystal(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, health=0):
        super().__init__()
        self.image = pygame.image.load(filename).convert_alpha()
        self.image = pygame.transform.scale(self.image, (39, 48))
        self.rect = self.image.get_rect(center=(x, y))

        self.Health: int = health

        self.add(crystal_group)
