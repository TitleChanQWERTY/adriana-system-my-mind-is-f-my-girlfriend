from random import randint

import pygame
from group_config import particle_group


class PhysicalParticle(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, scale, force_fall):
        super().__init__()
        self.image = pygame.image.load(filename).convert_alpha()
        self.image = pygame.transform.scale(self.image, (scale, scale))
        self.rect = self.image.get_rect(center=(x, y))

        self.add(particle_group)

        self.forceY: int = randint(-35, -15)
        self.forceFall: int = force_fall

        self.forceX: int = randint(-9, 9)

    def update(self):
        if self.rect.y > 740:
            self.kill()

        self.forceY += self.forceFall

        self.rect.y += self.forceY
        self.rect.x += self.forceX


def createParticle(x, y, filename, size, count, force_fall=1):
    for i in range(count):
        PhysicalParticle(x, y, filename, size, force_fall)
