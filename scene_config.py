from random import randint

from config_script import WINDOW_SIZE
from player import Player
from scene_item import crystal

player = Player.Player(550, WINDOW_SIZE[1] - 35, 14)

SCENE: int = 0
LEVEL: int = 0

numberCrystal: int = 0
maxCrystal: int = 9
maxCreateCrystal: int = 5


def createCrystal(health, number_create_x, width_row):
    global numberCrystal, maxCrystal
    width_x = WINDOW_SIZE[0] // 2 // width_row
    for number_create_i in range(number_create_x):
        crystal_x = width_x + width_x * number_create_i
        if crystal_x < 20 or crystal_x > 1256:
            crystal_x = randint(25, 1255)
        width_y = randint(135, 455)
        crystal.Crystal(crystal_x - 1, width_y, "ASSETS/sprite/crystal/red.png", health)
    numberCrystal = number_create_x


def changerLvl():
    createCrystal(0, maxCreateCrystal, 1)
