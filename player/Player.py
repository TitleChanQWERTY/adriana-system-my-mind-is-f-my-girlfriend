import pygame
from config_script import WINDOW_SIZE, sc
from animator import Animator
from player import ball


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y, speed):
        super().__init__()
        self.image = pygame.image.load("ASSETS/sprite/player/stay/1.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (64, 64))
        self.rect = self.image.get_rect(center=(x, y))

        self.imageHeart = pygame.image.load("ASSETS/sprite/player/heart.png").convert_alpha()
        self.imageHeart = pygame.transform.scale(self.imageHeart, (37, 37))
        self.rectHeart = self.imageHeart.get_rect(center=(WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] - 30))

        self.HeartCount: int = 3
        self.PlayerCount: int = 3
        self.Score: int = 0

        self.forceUp = 12
        self.ground: int = 717
        self.moveUp = self.forceUp + 1

        self.speed = speed

        self.fileStay = ("ASSETS/sprite/player/stay/1.png", "ASSETS/sprite/player/stay/2.png")

        self.fileMove = ("ASSETS/sprite/player/move/1.png", "ASSETS/sprite/player/move/2.png",
                         "ASSETS/sprite/player/move/3.png")

        self.animStay = Animator(25, self.fileStay)
        self.animMove = Animator(6, self.fileMove)

        self.countSpit: int = 1

    def update(self):
        key = pygame.key.get_pressed()

        if key[pygame.K_k] and self.countSpit > 0:
            self.countSpit -= 1
            if key[pygame.K_a]:
                ball.Ball(self.rect.centerx, self.rect.centery - 65, -1)
            elif key[pygame.K_d]:
                ball.Ball(self.rect.centerx, self.rect.centery - 65, 1)
            else:
                ball.Ball(self.rect.centerx, self.rect.centery - 65, 0)

        self.startAnim(key)
        self.move(key)

        sc.blit(self.imageHeart, self.rectHeart)

    def move(self, key):
        move_direct_x = key[pygame.K_d] - key[pygame.K_a]

        self.rect.x += round(move_direct_x * self.speed)

        self.rect.x = max(0, min(WINDOW_SIZE[0] - 64, self.rect.x))

        if key[pygame.K_d]:
            self.image = pygame.transform.flip(self.image, True, False)

        if key[pygame.K_SPACE] and self.ground == self.rect.bottom:
            self.moveUp = -self.forceUp

        if self.moveUp <= self.forceUp:
            if self.rect.bottom + self.moveUp < self.ground:
                self.rect.bottom += self.moveUp
                if self.moveUp < self.forceUp:
                    self.moveUp += 1
            else:
                self.rect.bottom = self.ground
                self.moveUp = self.forceUp + 1

    def startAnim(self, key):
        if key[pygame.K_d] or key[pygame.K_a]:
            self.image = self.animMove.update()
        else:
            self.posHeartX = self.rect.centerx
            self.image = self.animStay.update()
        self.image = pygame.transform.scale(self.image, (64, 64))
