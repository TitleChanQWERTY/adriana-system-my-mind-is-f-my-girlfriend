from random import randint

import pygame
from config_script import WINDOW_SIZE
from group_config import ball_group


class Ball(pygame.sprite.Sprite):
    def __init__(self, x, y, dXMove):
        super().__init__()
        self.image = pygame.image.load("ASSETS/sprite/player/spit.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (35, 39))
        self.rect = self.image.get_rect(center=(x, y))
        self.add(ball_group)

        self.sizeSprite: int = 30

        self.x, self.y = x, y
        self.dX, self.dY = dXMove, -1

        self.speed: int = randint(7, 10)

        self.angleRot: int = 0

    def update(self):
        self.y += self.speed * self.dY
        self.x += self.speed * self.dX
        if self.x < self.sizeSprite or self.x > WINDOW_SIZE[0] - self.sizeSprite:
            self.dX = -self.dX
        if self.y < self.sizeSprite+97 or self.y > WINDOW_SIZE[1] - self.sizeSprite:
            self.speed += randint(0, 1)
            self.dY = -self.dY

        self.image = pygame.image.load("ASSETS/sprite/player/spit.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (52, 52))
        self.angleRot += self.speed+3
        self.image = pygame.transform.rotate(self.image, self.angleRot)
        self.rect = self.image.get_rect(center=(self.x, self.y))
